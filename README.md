# usb control CAEN N1470 hv

USB control and monitoring of high voltage power supply NIM standard CAEN N1470

Using communication protocol RS485 and python module _serial_ it is possible to control power supply N1470 from CAEN. 
_serial_ provides all the protocol while extra clasess written in python allow easy control of it.
The GUI and monitoring is build under _PyQt5_ and _pyqtgraph_ for better graphics control.

